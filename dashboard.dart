import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(
          'Dashboard',
          style: TextStyle(color: Colors.white),
        )),
        body: GridView.count(
            primary: false,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            children: <Widget>[
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                color: Colors.blue,
                child: ElevatedButton(
                    child: const Text('Feature Screen 1'),
                    onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const Text('Welcome Page')))
                        }),
              ),
              Container(
                  padding: const EdgeInsets.all(8),
                  color: Colors.teal[200],
                  child: ElevatedButton(
                      child: const Text('Feature Screen 2'),
                      onPressed: () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const Text('My Vision')))
                          }))
            ]));
  }
}
